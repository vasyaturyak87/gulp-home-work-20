const {src, dest, series, watch} = require('gulp')
const scss = require('gulp-sass')
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const pug = require('gulp-pug');
const rename = require("gulp-rename");
const connect = require('gulp-connect')

const appPath = {
    html: './src/index.html',
    scss: './src/scss/**/*.scss',
    js: './src/js/*.js',
    img: [
        './src/img/**/*.jpg',
        './src/img/**/*.png'
    ]
}
const destPath = {
    html: './dist',
    css: './dist/css/',
    js: './dist/js/',
    img: './dist/img/'
}

function image() {
    return src(appPath.img)
        .pipe(imagemin())
        .pipe(rename(function (path) {
            return {
                dirname: path.dirname,
                basename: path.basename.substr(0, 3) + "-test",
                extname: path.extname
            };
        }))
        .pipe(dest(destPath.img))
        .pipe(connect.reload())
}

function scssCompress() {
    return src(appPath.scss)
        .pipe(scss({
            // outputStyle: 'compressed'
        }))
        .pipe(dest(destPath.css))
        .pipe(connect.reload())
}

function pugToHtml() {
    return src('./src/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(dest('./dist/'))
        .pipe(connect.reload())
}


function html() {
    return src(appPath.html)

        .pipe(dest(destPath.html))
        .pipe(connect.reload())
}

function jsMin() {
    return src(appPath.js)
        .pipe(uglify())
        .pipe(dest(destPath.js))
}

function server() {
    connect.server({
        name: 'Dev App',
        root: 'dist',
        port: 8080,
        livereload: true
    })
}

//watch('src/*.pug', pugToHtml);

watch(appPath.html, html);
watch(appPath.scss, scssCompress);
watch(appPath.js, jsMin);
watch(appPath.img, {events: 'add'}, image);

exports.default = series(scssCompress, image, html, jsMin, server)

//exports.default = series(scssCompress, image, pugToHtml, jsMin, server)